---
id: sources
title: Sources & bibliography
authors: Daniel Nomadic
---

## Sources

https://id4d.worldbank.org/global-dataset

https://blogs.worldbank.org/voices/global-identification-challenge-who-are-1-billion-people-without-proof-identity

https://id4d.worldbank.org/sites/id4d.worldbank.org/files/2018-08/ID4D%20Data%20Notes%20revised%20082918.pdf

https://query.prod.cms.rt.microsoft.com/cms/api/am/binary/RE2DjfY

https://www.microsoft.com/en-us/security/business/identity-access-management/decentralized-identity-blockchain

https://consensys.net/blockchain-use-cases/digital-identity/

https://esource.dbs.ie/handle/10788/3647

https://www.cs.bgu.ac.il/~frankel/TechnicalReports/2016/16-02.pdf

https://www.tandfonline.com/doi/full/10.1080/14650045.2020.1823836

https://documents.worldbank.org/en/publication/documents-reports/documentdetail/727021583506631652/global-id-coverage-barriers-and-use-by-the-numbers-an-in-depth-look-at-the-2017-id4d-findex-survey

https://documents.worldbank.org/en/publication/documents-reports/documentdetail/248371559325561562/id4d-practitioner-s-guide

https://www.mastercard.com/news/europe/en-uk/newsroom/press-releases/en-gb/2019/march/new-mobile-money-propositions-have-the-potential-to-reduce-the-world-s-unbanked-population-by-more-than-a-third/

http://financial-inclusion.com/wp-content/uploads/2019/03/MCC1-FinInc-Report-Screen.pdf

https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3785452

https://www.adaltas.com/fr/2019/01/23/identites-auto-souveraines/

## Bibliography

Consenys. “Blockchain in Digital Identity.” https://consensys.net/blockchain-use-cases/digital-identity/. Accessed June 2021.

Dib, Omar; Toumi, Khalifa. Decentralized Identity Systems: Architecture, Challenges, Solutions and Future Directions. 2020, https://papers.ssrn.com/sol3/papers.cfm?abstract_id=3785452.

Lord, Nate. “Uncovering Password Habits: Are Users’ Password Security Habits Improving? (Infographic).” DATA INSIDER, Digital Guardian's blog, 29 September 2020: https://digitalguardian.com/blog/uncovering-password-habits-are-users-password-security-habits-improving-infographic. Accessed 23 July 2021.

Metz,Anna Zita; Clark,Julia Michal.2019. Global ID Coverage, Barriers, and Use by the Numbers: An In-Depth Look at the 2017 ID4D-Findex Survey. 2019: https://documents.worldbank.org/en/publication/documents-reports/documentdetail/727021583506631652/global-id-coverage-barriers-and-use-by-the-numbers-an-in-depth-look-at-the-2017-id4d-findex-survey.

Microsoft. Decentralized Identity, Own and control your identity. 2018: https://www.microsoft.com/en-us/security/business/identity-access-management/decentralized-identity-blockchain.

Simon Hardie. Unravelling the web of inclusion. Mastercard, 2019: http://financial-inclusion.com/wp-content/uploads/2019/03/MCC1-FinInc-Report-Screen.pdf.
van Bokkem, D., Hageman, R., Koning, G., Nguyen, L. and Zarin, N.,. Self-sovereign identity solutions: The necessity of blockchain technology. 2019, https://arxiv.org/pdf/1904.12816.pdf.

VYJAYANTI T DESAI; ANNA DIOFASI; JING LU. “The global identification challenge: Who are the 1 billion people without proof of identity?” 2018,
https://blogs.worldbank.org/voices/global-identification-challenge-who-are-1-billion-people-without-proof-identity.

World Bank Group. ID4D Practitioner’s Guide. 2019, https://documents.worldbank.org/en/publication/documents-reports/documentdetail/248371559325561562/id4d-practitioner-s-guide  